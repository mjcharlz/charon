package com.mj.cheechee;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Frank on 01-Feb-18.
 */

public class BoardView extends View {

    private static final String TAG = "dope";
    private final Context context;
    private int rows, columns;
    private int[] values;
    private Random random = new Random();
    private int spread;
    private float boxWidth, boxHeight;
    private boolean[][] boxChecked;
    private int[][] boxValues;
    private Paint blackPaint, bluePaint, redPaint;

    private int selectedValue = -1;
    private int score = 0;

    public BoardView(Context context) {
        super(context);
        this.context = context;
    }

    public BoardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public BoardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    public BoardView(Context context, int rows, int columns) {
        super(context);
        this.rows = rows;
        this.columns = columns;
        this.spread = rows * columns;
        this.context = context;

        Typeface font = Typeface.createFromAsset( context.getAssets(), "opensans.ttf");


        blackPaint = new Paint();
        blackPaint.setColor(Color.BLACK);
        blackPaint.setTextSize(64f);
        blackPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        blackPaint.setTypeface(font);


        bluePaint = new Paint();
        bluePaint.setColor(Color.BLUE);
        bluePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        redPaint = new Paint();
        redPaint.setARGB(32, 34, 12, 200);
        redPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        calculateDimensions();


        boxValues = new int[rows][columns];
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                boxValues[i][j] = random.nextInt(spread);
            }
        }


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        MyApp.log("on size changed");
        super.onSizeChanged(w, h, oldw, oldh);
        calculateDimensions();
    }


    private void calculateDimensions() {
        MyApp.log("calculating dimensions");
        if (rows < 1 || columns < 1) {
            return;
        }

        boxWidth = getWidth() / columns;
        boxHeight = getHeight() / rows;

        MyApp.log(columns+" -cd- "+boxWidth);
        MyApp.log(rows+" -cd- "+boxHeight);

        if (boxHeight <= boxWidth)
            boxWidth = boxHeight;
        else
            boxHeight = boxWidth;




        boxChecked = new boolean[columns][rows];
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        MyApp.log("on measure");

        int desiredWidth = (int) (columns * boxWidth);
        int desiredHeight = (int) (rows * boxHeight);

        if (desiredHeight == 0 || desiredWidth == 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        MyApp.log(desiredHeight+" --- "+desiredWidth);

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        MyApp.log("on draw called");

        canvas.drawColor(Color.WHITE);

        if (columns == 0 || rows == 0) {
            return;
        }

        int width = getWidth();
        int height = getHeight();

        String text;
        float textWidth = 0;

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {

                text = ""+boxValues[i][j];
                textWidth = blackPaint.measureText(text);

                canvas.drawText(
                        text,
                        (i + 0.5f)*boxWidth - textWidth/2,
                        (j + 0.5f)*boxHeight + 18,
                        blackPaint
                );

                //checked box
                if (boxChecked[i][j]) {
                    canvas.drawRect(
                            i * boxWidth,
                            j * boxHeight,
                            (i + 1) * boxWidth,
                            (j + 1) * boxHeight,
                            redPaint);
                }
            }
        }


        for (int i = 1; i < columns; i++) {
            canvas.drawLine(i * boxWidth, 0, i * boxWidth, height, blackPaint);
        }

        for (int i = 1; i < rows; i++) {
            canvas.drawLine(0, i * boxHeight, width, i * boxHeight, blackPaint);
        }

        canvas.drawLine(columns * boxWidth, 0, columns * boxWidth, height, blackPaint);
        canvas.drawLine(0, rows * boxHeight, width, rows * boxHeight, blackPaint);
    }


    int previousRowCol = -1;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int column = (int)(event.getX() / boxWidth);
            int row = (int)(event.getY() / boxHeight);

            //beautiful
            //boxChecked[column][row] = !boxChecked[column][row];


            if (previousRowCol == 10000 * row + column) {
                MyApp.log("clicking on the same shit..");
                return  true;
            } else {
                previousRowCol = 10000 * row + column;
            }

            resetChecked();
            boxChecked[column][row] = true;

            if (selectedValue == boxValues[column][row]) {
                score += selectedValue;
                selectedValue = -1;
                refreshValues();
                resetChecked();
            } else {
                selectedValue = boxValues[column][row];
            }

            MyApp.log("selected value: "+selectedValue);
            MyApp.log("Score : "+score);

            invalidate();
        }

        return true;
    }

    private void resetChecked() {
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                boxChecked[i][j] = false;
            }
        }
    }

    private void refreshValues() {
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                boxValues[i][j] = random.nextInt(spread);
            }
        }
    }

    public int getScore() {
        return score;
    }
}
