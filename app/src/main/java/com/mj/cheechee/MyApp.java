package com.mj.cheechee;

import android.app.Application;
import android.util.Log;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Frank on 18-May-18.
 */

public class MyApp extends Application {

    private static final String FONT_PATH = "opensans.ttf";

    @Override
    public void onCreate() {
        super.onCreate();


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(FONT_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public  static void log(String message) {
        Log.e("Charon", message);
    }
}
