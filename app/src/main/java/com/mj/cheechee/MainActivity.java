package com.mj.cheechee;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout container = findViewById(R.id.container);

        BoardView boardView = new BoardView(this, 4, 4);

        container.addView(boardView);

        final TextView tvScore = findViewById(R.id.score);
        tvScore.setText("Score: 0\n"+
        "Touches: 0");

        boardView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                int score = ((BoardView)view).getScore();
                tvScore.setText("Score: "+score+"\n"+
                        "Touches: 0");

                return false;
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase)); //for fonts
    }



}
